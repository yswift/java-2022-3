package w12_2;

import java.util.ArrayList;

import w8_1.Animal;
import w8_1.Dog;

public class GDemo2_1 {
	public static void main(String[] args) {
//		Integer 是 Nubmer 的子类
		Number n = Integer.valueOf(123);
//		Integer[] 是 Number[] 的子类
		Number[] ns = new Integer[10];
//		但ArrayList<Integer> 不是 ArrayList<Number>的子类
		ArrayList<Number> nsl = new ArrayList<Integer>();
//
//		Dog 是 Animal 的子类
		Animal a = new Dog();
//		Dog[] 是 Animal[] 的子类
		Animal[] as = new Dog[10];
//		但ArrayList<Dog> 不是 ArrayList<Animal>的子类
		ArrayList<Animal> ass = new ArrayList<Dog>();

	}

}
