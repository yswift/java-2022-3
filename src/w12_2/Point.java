package w12_2;

class Point<T>{// 此处可以随便写标识符号
    private T x ;  
    private T y ;
    
    public void setX(T x){//作为参数
        this.x = x ;
    }
    public void setY(T y){
        this.y = y ;
    }
    public T getX(){//作为返回值
        return this.x ;
    }
    public T getY(){
        return this.y ;
    }
}
