package w12_2;

import java.util.ArrayList;

import w11.Person;

public class GDemo {
	public static void main(String[] args) {
		// 注意区分下面两种ArrayList声明方式的不同
//		ArrayList<String> list = new ArrayList<>();
		ArrayList list = new ArrayList();
		
		list.add("张飞");
		list.add(100);
		list.add(new Person("刘备", 1.7, 65));
		
		Object o1 = list.get(0);
		Person p = (Person) list.get(2);
		
		for (Object s : list) {
			String v = (String)s;
			System.out.println(s);
		}
	}
}
