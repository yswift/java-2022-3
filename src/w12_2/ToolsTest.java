package w12_2;

public class ToolsTest {
	public static void main(String[] args) {
        MyTools<Info>  myTools=new MyTools<Info>();
        
        Info i1 = new Info("清华大学","中国排名前2名");
        Info i2 = new Info("北京大学","中国排名前2名");
        myTools.print(i1, i2);
        System.out.println();
        
        // 注意区别 MyTools，MyTools2的不同
        MyTools<String>  mt1 =new MyTools<>();
        mt1.print("字符串1", "字符串2");
        
        MyTools2  mt2 =new MyTools2();
        mt2.print("字符串3", "字符串4");
    }

}
