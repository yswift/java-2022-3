package w12_2;

import java.util.ArrayList;

import w8_1.Animal;
import w8_1.Cat;
import w8_1.Dog;
import w8_1.Feline;

public class GDemo4 {
	static void f1() {
		ArrayList<Number> list = new ArrayList<>();
		list.add(Long.valueOf(100L));
		list.add(Double.valueOf(10.5));
		
		ArrayList<Integer> listInt = new ArrayList<>();
		ArrayList<? extends Number> listNum = listInt;
		// 可以取元素
		Number n = listNum.get(0);
		// <? extends Number>集合 不能添加元素
		listNum.add(Long.valueOf(100L));
		listNum.add(Double.valueOf(10.5));
	}
	
	static void f2() {
		ArrayList<Animal> listAnimal = new ArrayList<>();
		listAnimal.add(new Dog());
		
		ArrayList<? super Feline> listFeline = listAnimal;
		// 可以加元素
		listFeline.add(new Cat());
		// 但不能取元素
		Feline c1 = listFeline.get(0);
	}
	
	static void f3() {
		ArrayList<Animal> listAnimal = new ArrayList<>();
		listAnimal.add(new Dog());
		
		ArrayList<?> listFeline = listAnimal;
		// 不能加元素
		listFeline.add(new Cat());
		// 不能取元素
		Feline c1 = listFeline.get(0);
	}
}
