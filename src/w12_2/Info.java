package w12_2;

class Info{
    String name; String  desp;
    
   public Info(String name,String desp){
       this.name=name; this.desp=desp;
   }
   
   @Override
   public String toString() {
       return "Info{" +
               "name='" + name + '\'' +
               ", desp='" + desp + '\'' +
               '}';
   }
}

