package w12_2;

import java.util.ArrayList;

public class GDemo2 {
	static void f0(Object o) {
		System.out.println(o);
	}
	
	static void f1(ArrayList<Object> list) {
		System.out.println(list);
	}

	public static void main(String[] args) {
		String s1 = "123";
		f0(s1);
		
		ArrayList<String> list = new ArrayList<>();
		f1(list);
	}

}
