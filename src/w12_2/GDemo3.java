package w12_2;

import java.util.ArrayList;

public class GDemo3 {
	// 对元素求和
	static double sum(ArrayList<? extends Number> list) {
//	static double sum(ArrayList<Number> list) {
		double sum = 0;
		for (Number n : list) {
			sum += n.doubleValue();
		}
		return sum;
	}
	
	public static void main(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<>();
		list1.add(10);
		list1.add(20);
		list1.add(30);
		System.out.println(list1);
		
		double sum = sum(list1);
		System.out.println("sum1 = " + sum);
		
		ArrayList<Double> list2 = new ArrayList<>();
		list2.add(100.0);
		list2.add(200.0);
		list2.add(300.0);
		System.out.println(list2);
		double sum2 = sum(list2);
		System.out.println("sum2 = " + sum2);
	}

}
