package w12_2;

import java.util.ArrayList;

public class GDemo1_1 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("刘备");
		list.add("关羽");
		list.add("张飞");
		
		for (String s : list) {
			System.out.println(s);
		}
		
		System.out.println();
		ArrayList list2 = new ArrayList<>();
		list2.add("刘备");
		list2.add("关羽");
		list2.add("张飞");
		
		for (Object o: list2) {
			String s = (String) o;
			System.out.println(s);
		}
	}

}
