package w12_2;

public class PointTest {
	public static void main(String[] args) {
        //2.使用泛型类创建对象时，泛型类中的占位符T必须要用真实的数据类型代替。
        Point<Integer>  p1=new Point<Integer>();
        p1.setX(10);
        p1.setY(20);
        System.out.println("整数点X坐标是："+p1.getX()+"整数点Y坐标是："+p1.getY());
        
        Point<Float> p2=new Point<Float>();
        p2.setX(10.1F);
        p2.setY(20.2F);
        System.out.println("浮点数点X坐标是："+p1.getX()+"浮点数点Y坐标是："+p1.getY());
    }

}
