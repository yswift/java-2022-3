package sy.sy12_1;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
	// 生成count个随机的学生信息
    static List<Student> newStudents(int count) {
        return Stream.generate(Student::new).limit(count).collect(Collectors.toList());
    }
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// 从键盘输入数组元素和随机数种子
		int seed = reader.nextInt();
		Student.r = new Random(seed);
		
		List<Student> studentList = newStudents(5);
		
		Test.printAgeThan19(studentList);
	}
}

class Student {
	static Random r;
	static int id = 1;
	static String[] majors = { "计科", "机械", "电气", "通信", "信息安全" };
	// 学号
//	String no;
	// 姓名
	String name;
	// 专业
	String major;
	// 年龄
	int age;

	Student() {
		
		// 使用随机值初始化类
//		no = String.format("20%02d%02d%06d", 10 + r.nextInt(10), r.nextInt(10), r.nextInt(200));
		name = "学生" + id++;
		major = majors[r.nextInt(majors.length)];
		age = 18 + r.nextInt(10);
	}

	@Override
	public String toString() {
		return "Student{name='" + name + '\'' + ", major='" + major + '\'' + ", age=" + age
				+ '}';
	}
}
//====== 以上代码请勿修改

// ==== 从这里开始编写你的代码
class Test {
	// 输出年龄大于19学生，参见 b2.StreamE2 类
	static void printAgeThan19(List<Student> list) {
		System.out.println("输出年龄大于19的学生");
	}

}