package sy.sy10_3;

import java.util.Random;
import java.util.Scanner;

//=== 以下代码请勿修改，你要完成的代码在下面
class Person {
	// 3个私有的实例变量
	// 姓名
	private String name;
	// 身高
	private double height;
	// 体重
	private double weight;
	
	// 有三个参数的构造方法
	public Person(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}

	// 每个变量的 getter/setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
}

public class Main {
	// 用随机数初始化人的身高，体重
	static void init(Person[] people, int seed) {
		// 身高随机数
		Random rh = new Random(seed);
		// 体重随机数
		Random rw = new Random(seed*2);
		// 姓名数组
		String[] names = {"张三", "李四", "王五", "赵六"};
		
		for (int i = 0; i<people.length; i++) {
			// 身高: 1.5 - 2.0
			// 体重: 50 - 100
			people[i] = new Person(names[i], (150 + rh.nextInt(50))/100.0, (50 + rw.nextInt(50)));
		}
	}
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// 从键盘输入数组元素和随机数种子
		int seed = reader.nextInt();
		
//		0、定义并创建4个元素的Person型数组
		Person[] people = new Person[4];
		init(people, seed);
		
//		1、遍历数组元素（从0开始访问数组的每个元素），并输出姓名,身高,体重
		System.out.println("1、遍历数组元素（从0开始访问数组的每个元素），并输出姓名");
		Test.print(people);
		System.out.println();
		
//		2、求数组的平均体重
		System.out.println("2、求数组的平均体重");
		double avg = Test.weightAvg(people);
		System.out.printf("avg = %.2f\n", avg);
		System.out.println();	
		
//		3、找出数组中体重最轻的人
		System.out.println("3、找出数组中体重最轻的人");
		Person p = Test.weightMin(people);
		System.out.println("体重最轻的是: " + p.getName() + ", 体重: " + p.getWeight());
		System.out.println();
		
//		4、在数组中查找体重大于给定值的第一个人，找到返回该人，没找到返回null
		System.out.println("4、在数组中查找体重大于给定值的第一个人，找到返回该人，没找到返回null");
		System.out.println("在数组中查找体重大于60的人");
		Person p1 = Test.find(people, 60);
		System.out.println("找到体重大于60的人 = " + (p1 == null? "" : p1.getName()));
		
		System.out.println("在数组中查找体重大于95的人");
		Person p2 = Test.find(people, 95);
		System.out.println("找到体重大于95的人 = " + (p2 == null? "" : p2.getName()));
		System.out.println();
	}
}


//=== 从这里编写代码
class Test {
//	1、遍历数组元素（从0开始访问数组的每个元素），并输出Person的姓名,身高,体重，注意中间用英文逗号分隔
	static void print(Person[] people) {
		for (int i = 0; i < people.length; i++) {
			Person p = people[i];
			System.out.println(p.getName() + "," + p.getHeight() + "," + p.getWeight());
		}
	}
	
//	2、求数组的平均体重
	static double weightAvg(Person[] people) {

	}
	
//	3、找出数组中体重最轻的人
	static Person weightMin(Person[] people) {

	}
	
//	6、在数组中查找体重大于给定值的第一个人，找到返回该人，没找到返回null
	static Person find(Person[] people, double weight) {

	}
}
//=== 代码编写结束
