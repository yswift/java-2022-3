package sy.sy7_1;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * 图形类为父类，包含边长信息
 */
abstract class Shape {
	private double length;
    
    public Shape(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public abstract double getArea();
}
//=== 从这里开始编写你的代码
// 1. 编写正三角形类 RegularTriangle 继承 Shape 类，构造方法为RegularTriangle(double length), 实现抽象方法 getArea
class RegularTriangle extends Shape {
	public RegularTriangle(double length) {
		// 填写代码
		super(length);
	}
	
	// 计算正三角形类面积，并返回，提示：使用海伦公
	public double getArea() {
		// 填写代码
		double l = getLength();
		return 0.5 * l * (l * 0.5 * Math.sqrt(3));
	}
}

// 2.  编写圆形类 Circular 继承 Shape 类，实现抽象方法 getArea
class Circular extends Shape {

	public Circular(double length) {
		super(length);
	}

	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}

// 3. 编写正方形类 Square  继承 Shape 类，实现抽象方法 getArea

//=== 代码编写结束
public class Main {
	static class TriangleFunction implements Function<Double, Shape> {
		@Override
		public Shape apply(Double length) {
			return new RegularTriangle(length);
		}
	}
	
	static HashMap<String, Function<Double, Shape>> name2Shape = new HashMap<>();
	static {
		// 1. 使用内部类
		Function<Double, Shape> f = new TriangleFunction();
		name2Shape.put("正三角形", f);
		
		// 2. 使用匿名内部类
		Function<Double, Shape> f2 = new Function<Double, Shape>() {
			@Override
			public Shape apply(Double length) {
				return new RegularTriangle(length);
			}
		};
		name2Shape.put("正三角形", f2);
		
		// 3. 使用匿名内部类,且无中间变量
		name2Shape.put("正三角形", new Function<Double, Shape>() {
			@Override
			public Shape apply(Double length) {
				return new RegularTriangle(length);
			}
		});
		
		// 4. 使用 lambda 表达式
		name2Shape.put("正三角形", (length) -> new RegularTriangle(length));
		name2Shape.put("圆形", (length) -> new Circular(length));
//		name2Shape.put("正方形", (length) -> new Square(length));
	}
	
	public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String name = reader.next();
        if (name == null || name.isEmpty()) {
          	return;
        }
        Function<Double, Shape> shapeCreator = name2Shape.get(name);
        if (shapeCreator == null) {
            System.out.println("Can't calculate");
            return;
        }
        double length = reader.nextDouble();
        Shape s = shapeCreator.apply(length);
        System.out.printf("面积为： %.2f\n", s.getArea());
    }
	
}
