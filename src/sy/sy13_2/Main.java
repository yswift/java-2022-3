package sy.sy13_2;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Random;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws IOException {
		String[] lines = { 
				"  莫听穿林打叶声，",
				"何妨吟啸且徐行。    ",
				"竹杖芒鞋轻胜马，",
				"谁怕？",
				"一蓑烟雨任平生。   ", 
				"料峭春风吹酒醒，",
				"微冷，",
				"山头斜照却相迎。   ", 
		};
		String blank = "              ";

		// 读取随机数种子
		Scanner reader = new Scanner(System.in);
		int seed = reader.nextInt();
		Random random = new Random(seed);
		
		// 随机写入字符串
		StringWriter sw = new StringWriter();
		int bl = blank.length();
		for (int i=0; i<4; i++) {
			int lineNumber = random.nextInt(lines.length);
			// 写前空格
			sw.write(blank.substring(random.nextInt(lines.length/2)));
			// 写字符
			sw.write(lines[lineNumber]);
			// 写后空格
			sw.write(blank.substring(random.nextInt(lines.length/2)));
			// 写换行
			sw.write("\n");
		}
		// 把字符串转为 byte 数组
		byte[] bytes = sw.toString().getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		
		// 读取并显示
		Test.readString(bis);
	}

}

//==== 开始编写代码
class Test {
	/**
	 * 编写方法，从 InputStream is 中 readLine() 方法读取一行字符串，并用字符串的trim()方法删除前后的空格，后输出
	 * 
	 * 提示: 
	 * 1）先把字节流转换成字符流 
	 * 2）再把字符流包装成 BufferedReader 
	 * 3）用BufferedReader 类的 readLine()方法来读取一行字符串，直到读取到的字符串为null 
	 * 4）用字符串的trim()方法删除前后的空格，后输出
	 * 
	 * @param br
	 * @throws IOException 
	 */
	public static void readString(InputStream is) throws IOException {
		// 1）先把字节流转换成字符流
		// 2）再把字符流包装成 BufferedReader
		// 3）用BufferedReader 类的 readLine() 方法来读取一行字符串，直到读取到的字符串为null
			// 4）用字符串的trim()方法删除前后的空格，后输出
	}
}
//==== 结束编写代码
