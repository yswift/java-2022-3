package sy.sy5_1;
//实验5-1的模板，复制到网页后删除上一行: package sy...;

import java.util.Scanner;

//==== 从这里开始写你的代码
class Point {
	// === 两个private的实例变量：
	// 点的横坐标 x，double类型
	// 点的纵坐标 y，double类型

	// 默认的构造方法，即没有参数的构造方法
	
	// 以横坐标，纵坐标为参数的构造方法
	public Point(double x, double y) {

	}
	
	// 横坐标 x 的getter和setter
	public double getX() {
	}

	public void setX(double x) {
	}

	// 纵坐标 y 的getter和setter

	// 求两点间距离的方法
	/**
	 * 求两点间距离, 计算从自己到参数p点之间的距离。
	 * 平方根计算使用方法 Math.sqrt，参
	 * 见：https://www.apiref.com/java11-zh/java.base/java/lang/Math.html#sqrt(double)
	 * @param p 另一个点
	 * @return 两点间距离
	 */
	public double distance(Point p) {
	}
}
//==== 你的代码编写结束


public class Main {
	public static void main(String[] args) {
		// 点 p1(1, 1)
		Point p1 = new Point();
		p1.setX(1);
		p1.setY(1);
		
		// 点 p2
		Scanner reader = new Scanner(System.in);
		double x2 = reader.nextDouble();
		double y2 = reader.nextDouble();
		Point p2 = new  Point(x2, y2);
		
		// 计算p1到p2的距离
		double d1 = p1.distance(p2);
		System.out.printf("%.2f\n", d1);
		// 计算p2到p1的距离
		double d2 = p2.distance(p1);
		System.out.printf("%.2f\n", d2);
	}
}
