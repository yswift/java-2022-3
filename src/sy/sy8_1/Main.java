package sy.sy8_1;

import java.util.Scanner;

//=== 开始编写你的代码
//编写类Person,该类拥有：
class Person {
	// 1、私有实例变量
	// name(String 类型);//用于存放姓名；
	// age(int);//用于存放年龄，正常值:[0, 150]；
	// height(double); // 身高，单位:米，正常值:[0.5, 3]；
	// weight(double); // 体重，单位：kg，正常值:[5, 400]；

	// 2、为每个实例变量加上getter/setter
	// 在setter中加入检查机制，超出正常范围抛出异常

// 设置年龄示例
	public void setAge(int age) {
		if (age < 0 || age > 150) {
			throw new IllegalArgumentException("年龄超出正常范围[0, 150], age = " + age);
		}
		// 别忘了给实例变量age 赋值
	}

	// 3、在Person类中添加名为calcBMI的方法，用于计算并返回个人的BMI值，方法头如下
	// BMI = 体重公斤数除以身高米数平方
	public double calcBMI() {
	}
}
//=== 结束编写你的代码

//=== 以下的代码，请勿修改
public class Main {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String name = reader.next();
		int age = reader.nextInt();
		double height = reader.nextDouble();
		double weight = reader.nextDouble();
		try {
			Person p = new Person();
			p.setName(name);
			p.setAge(age);
			p.setHeight(height);
			p.setWeight(weight);
			System.out.printf("%s,%d岁,BMI = %.1f\n", p.getName(), p.getAge(), p.calcBMI());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
