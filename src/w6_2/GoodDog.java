package w6_2;

public class GoodDog {
	private int size;
	
	// setter
	public void setSize(int size) {
		if (size < 5) {
			throw new IllegalArgumentException("size < 5");
		}
		this.size = size;
	}
	
	// getter
	public int getSize() {
		return size;
	}
	
	void bark() {
		System.out.println("Ruff! Ruff!");
	}

}
