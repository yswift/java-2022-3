package w6_2;

public class Test {
	public static void main(String[] args) {
		CPU cpu = new CPU();
		cpu.setSpeed(2200);
		
		HardDisk hd = new HardDisk();
		hd.setAmount(200);
		
		PC pc = new PC();
		pc.setCPU(cpu);
		pc.setHardDisk(hd);
		
		pc.show();
	}

}
