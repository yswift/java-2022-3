package w6_2;

public class TestGoodDog {
	public static void main(String[] args) {
		GoodDog d = new GoodDog();
		//d.size = -1;
		// 赋值，调用setter
		d.setSize(-1);
		// 取值，调用getter
		System.out.println("size = " + d.getSize());
	}

}
