package w6_2;

public class Person {
	private String name;
	public Person() {
	}
	public Person(String n) {
		name = n;
	}
	
	// setter
	public void setName(String name) {
		this.name = name;
	}
	// getter
	public String getName() {
		return name;
	}
	
	void info() {
		System.out.println("my name = " + name);
	}

}
