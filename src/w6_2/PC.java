package w6_2;

public class PC {
	private CPU cpu;
	private HardDisk HD;
	
	public void setCPU(CPU cpu) {
		this.cpu = cpu;
	}
	public void setHardDisk(HardDisk hd) {
		HD = hd;
	}
	public void show() {
		System.out.println("CPU 速度: " + cpu.getSpeed());
		System.out.println("硬盘容量: " + HD.getAmount());
	}
	
}
