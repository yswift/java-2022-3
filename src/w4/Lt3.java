package w4;


public class Lt3 {
	static int max(int n1, int n2) {
		int result;
		if (n1 > n2) {
			result = n1;
		} else {
			result = n2;
		}
		return result;
	}
	
	static int f(int n) {
		if (n >= 0) {
			return 1;
		} 
		return 0;
	}
	
	public static void main(String[] args) {
		int i = 10;
		int j = 20;
		int k = max(i,j);
		
		System.out.println("k = " + k);
	}

}
