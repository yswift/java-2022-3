package w4;

public class Lt4 {
	// 参数传递
	
	public static void main(String[] args) {
		int a = 10;
		int b = a;
		a = 20;
		
		System.out.println("a = " + a);
		System.out.println("b = " + b);
	}

}
