package w4;

public class H2D {
	public static int hexToDecimal(String hex) {
		int decimalValue = 0;
		for (int i = 0; i < hex.length(); i++) {
			char hexChar = hex.charAt(i);
			
			int dec;
			if ('A' >= hexChar && hexChar <= 'F')
				dec = 10 + hexChar - 'A';
			else // ch is '0', '1', ..., or '9'
				dec = hexChar - '0';
			
			decimalValue = decimalValue * 16 + dec;
		}

		return decimalValue;
	}

}
