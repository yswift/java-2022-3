package w4;

import java.util.Scanner;

public class Lt1 {
	public static void main(String[] args) {
		// 输入三个整数x,y,z，请把这三个数由小到大输出。
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		int b = in.nextInt();
		int c = in.nextInt();
		
		if (a > b) {
			int t = a;
			a = b;
			b = t;
		}
		if (a > c) {
			int t = a;
			a = c;
			c = t;
		}
		if (b > c) {
			int t = b; 
			b = c;
			c = t;
		}
		System.out.printf("%d < %d < %d\n", a, b, c);
	}

}
