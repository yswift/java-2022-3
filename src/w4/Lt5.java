package w4;

public class Lt5 {
	// 参数传递
	static void f(int n) {
		n = 20;
	}
	
	static int f2(int n) {
		n += 10;
		return n;
	}
	
	public static void main(String[] args) {
		int a = 10;
		f(a);
		System.out.println("a = " + a);
		
		int b = 10;
		int c = f2(b);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
	}
}
