package w9;

public class TestChar {
	public static void main(String[] args) {
		char c = '\u6211';
		System.out.println(c);
		System.out.println(++c);
		
		c = 'A';
		System.out.printf("%d, %x, %c\n", (int)c, (int)c, c);
		c = 'a';
		System.out.printf("%d, %x, %c\n", (int)c, (int)c, c);
		c = '0';
		System.out.printf("%d, %x, %c\n", (int)c, (int)c, c);
		
	}

}
