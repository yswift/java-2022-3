package w13;

import java.io.*;
import java.util.*;

public class Example10_16 {
	public static void main(String args[]) {
		File file = new File("src\\w14", "student.txt");
		Scanner sc = null;
		int count = 0;
		double sum = 0;
		try {
			double score = 0;
			sc = new Scanner(file);
			sc.useDelimiter("[^0123456789.]+");
			while (sc.hasNextDouble()) {
				score = sc.nextDouble();
				count++;
				sum = sum + score;

				System.out.println(score);
			}
			double aver = sum / count;
			String str = String.format("%.3f", aver);
			System.out.println("平均成绩:" + str);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
