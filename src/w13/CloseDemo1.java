package w13;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CloseDemo1 {
	public static void main(String[] args) {
		// 显示的关闭流
		byte[] a = "新年快乐".getBytes();
		File file = new File("src\\w13", "CloseDemo1.txt"); // 输出的目的地
		
		OutputStream out = null;
		try {
			out = new FileOutputStream(file);
			out.write(a); // 向目的地写数据
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
