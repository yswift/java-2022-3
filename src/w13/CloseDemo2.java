package w13;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CloseDemo2 {
	public static void main(String[] args) {
		byte[] a = "新年快乐".getBytes();
		File file = new File("src\\w14", "CloseDemo1.txt"); // 输出的目的地
		// 自动关闭流
		try (OutputStream out = new FileOutputStream(file)) {
			out.write(a); // 向目的地写数据
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
