package w7;

public class ShapeTest3 {
	public static void main(String[] args) {
		System.out.println("Shape s = new Cicle()");
		Shape s = new Cicle();
		s.rotate();
		s.playSound();
		System.out.println("s = new Amoeba()");
		s = new Amoeba();
		s.rotate();
		s.playSound();
	}
}
