package w8_1;

public class InstTest {
	public static void main(String[] args) {
		Object d = new Dog();
		
		System.out.println("d is Animal? " + (d instanceof Animal));
		System.out.println("d is Canine? " + (d instanceof Canine));
		System.out.println("d is Dog? " + (d instanceof Dog));
		System.out.println("d is Pet? " + (d instanceof Pet));
		
//		d.play();
		if (d instanceof Dog) {
			Dog d1 = (Dog) d;
			d1.play();
		}
	}

}
