package w8_1;

public abstract class Feline extends Animal {
	public Feline() {
		System.out.println("making feline");
	}
	
	public void roam() {
		System.out.println("feline roam");
	}
}
