package w8_1;

public class Dog extends Canine implements Pet {
	@Override
	public void play() {
		System.out.println("dog play");
	}

	@Override
	public void beFriendly() {
		System.out.println("dog friendly");
	}

	@Override
	public void makeNoise() {
		System.out.println("dog make noise");
	}

	@Override
	public void eat() {
		System.out.println("dog eat");
	}

}
