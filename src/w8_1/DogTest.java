package w8_1;

public class DogTest {
	public static void main(String[] args) {
		Dog d = new Dog();
		d.eat();
		d.makeNoise();
		d.roam();
		d.sleep();
		d.play();
		d.beFriendly();
		
		Pet p = d;
//		p.eat();
//		p.roam();
		p.play();
		p.beFriendly();
		
		Animal a = d;
		a.play();
		a.eat();
		
		Object o = d;
		o.play();
		o.eat();
		
		
	}

}
