package w8_1;

public interface Pet {
	void play(); // 默认是 public 和 abstract
	
	public abstract void beFriendly();

}
