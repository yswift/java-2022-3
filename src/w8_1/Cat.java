package w8_1;

public class Cat extends Feline {
	public Cat() {
		super(); // 如果没写，编译器帮忙写一个
		System.out.println("making cat");
	}

	@Override
	public void makeNoise() {
		System.out.println("cat make noise");
	}

	@Override
	public void eat() {
		System.out.println("cat eat");
	}

}
