package w8_1;

public abstract class Animal {
	private String picture;
	private String food;
	private int hunger;
	private String boundaries;
	private String location;
	
	public abstract void makeNoise();
	
	public abstract void eat();
	
	public void sleep() {
		System.out.println("animal sleep");
	}
	public abstract void roam();

}
