package w8_1;

public class FinalVar {
	final int a = 0;
	final int b;
	
	public FinalVar() {
		b = 10;
	}
	
	final static void f() {
		
	}
	final static double v = Math.PI;

}
