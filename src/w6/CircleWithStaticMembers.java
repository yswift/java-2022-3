package w6;

public class CircleWithStaticMembers {
	/** The radius of the circle */
	double radius;

	/** The number of the objects created */
	static int numberOfObjects = 0;

	/** Construct a circle with radius 1 */
//	CircleWithStaticMembers() {
//		radius = 1.0;
//		numberOfObjects++;
//	}
	
	CircleWithStaticMembers() {
		this(1);
	}

	/** Construct a circle with a specified radius */
//	CircleWithStaticMembers(double newRadius) {
//		radius = newRadius;
//		numberOfObjects++;
//	}

	CircleWithStaticMembers(double radius) {
		this.radius = radius;
		numberOfObjects++;
	}

	/** Return numberOfObjects */
	static int getNumberOfObjects() {
		return numberOfObjects;
//		return radius;
	}

	/** Return the area of this circle */
	double getArea() {
		return radius * radius * Math.PI;
	}
}
