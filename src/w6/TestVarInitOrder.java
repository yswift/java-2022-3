package w6;

// 测试不同变量，初始化的次序
public class TestVarInitOrder {
	static TVar st1 = new TVar("静态变量赋值");
	TVar st2 = new TVar("实例变量赋值");
	
	static {
		// 静态代码块
		new TVar("执行静态块");
	}
	
	{
		new TVar("执行构造代码块");
	}
	
	TestVarInitOrder() {
		new TVar("执行构造方法");
	}
	
	public static void main(String[] args) {
		new TestVarInitOrder();
	}

}
