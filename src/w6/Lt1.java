package w6;

public class Lt1 {
	static void f1() {
		System.out.println("静态方法f1");
	}
	
	void f2() {
		System.out.println("非静态方法f2");
	}
	
	public static void main(String[] args) {
		f1();
		
		// f2(); 不能直接调用f2()
		// 为了调用f2(), 要先创建对象
		Lt1 lt1 = new Lt1();
		lt1.f2();
	}

}
