package b3;

import org.junit.Test;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamDemo1 {
    @Test
    public void t1() {
        Stream<Integer> iterate = Stream.iterate(0, (x) -> x + 2);
        iterate.limit(10).forEach(System.out::println);
    }

    @Test
    public void t2() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<20; i+=2) {
            list.add(i);
        }
        System.out.println(list);
    }

    @Test
    public void t3() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0; i<20; i+=2) {
            list.add(i);
        }
//        list.forEach(v -> System.out.println(v));
        list.forEach(System.out::println);
    }

    public List<Integer> generate(int n) {
        Stream<Integer> generate = Stream.generate(() -> (int)(Math.random()*10));
        return generate.limit(n).collect(Collectors.toList());
    }

    @Test
    public void dis() {
        List<Integer> list = generate(10);
        System.out.println("before");
        System.out.println(list);
        List<Integer> list2 = list.stream().distinct().collect(Collectors.toList());
        System.out.println("distinct");
        System.out.println(list2);
    }

    @Test
    public void filter() {
        List<Integer> list = generate(10);
        System.out.println("before");
        System.out.println(list);
        List<Integer> list2 = list.stream()
                .filter(v -> v % 2 ==0)
                .distinct()
                .collect(Collectors.toList());
        System.out.println("distinct");
        System.out.println(list2);
    }

    @Test
    public void map() {
        List<Integer> list = generate(10);
        System.out.println("before");
        System.out.println(list);
        List<Double> list2 = list.stream()
                .distinct()
                .filter(v -> v%2==0)
                .map(v -> Math.sqrt(v))
                .collect(Collectors.toList());
        System.out.println("distinct");
        System.out.println(list2);
    }

    @Test
    public void sort() {
        List<Integer> list = generate(10);
        System.out.println("before");
        System.out.println(list);
        List<Integer> list2 = list.stream()
//                .sorted(new Comparator<Integer>() {
//                    @Override
//                    public int compare(Integer o1, Integer o2) {
//                        return o2.intValue() - o1.intValue();
//                    }
//                })
                .sorted((o1, o2) -> o2 - o1)
                .collect(Collectors.toList());
        System.out.println("sort");
        System.out.println(list2);
        int a;
        Integer b;
    }

    @Test
    public void groupby1() {
        // 按照奇偶数分组
        List<Integer> list = generate(10);
        System.out.println("before");
        System.out.println(list);
        Map<Boolean, List<Integer>> m1 = list.stream()
                .collect(Collectors.groupingBy(v->v%2==0));
        System.out.println(m1);

        Map<Boolean, Long> m2 = list.stream()
                .collect(Collectors.groupingBy(v->v%2==0, Collectors.counting()));
        System.out.println(m2);

        Map<String, Long> m3 = list.stream()
                .collect(Collectors.groupingBy(v->v%2==0?"偶数":"奇数", Collectors.counting()));
        System.out.println(m3);
    }
}
