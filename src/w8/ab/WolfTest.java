package w8.ab;

public class WolfTest {
	public static void main(String[] args) {
		Animal w = new Wolf();
		
		w.makeNoise();
		w.eat();
		w.sleep();
		w.roam();
		
	}

}
