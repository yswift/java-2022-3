package w8;

public class WolfTest {
	public static void main(String[] args) {
		Animal w = new Wolf();
		
		w.makeNoise();
		w.eat();
		w.sleep();
		w.roam();
		
		System.out.println("new canine");
		w = new Canine();
		w.makeNoise();
		w.eat();
		w.sleep();
		w.roam();
	}

}
