package w8;

public class Animal {
	private String picture;
	private String food;
	private int hunger;
	private String boundaries;
	private String location;
	
	public void makeNoise() {
		System.out.println("animal make noise");
	}
	public void eat() {
		System.out.println("animal eate");
	}
	public void sleep() {
		System.out.println("animal sleep");
	}
	public void roam() {
		System.out.println("animal roam");
	}

}
