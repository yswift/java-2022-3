package b1;

public class Bat extends Animal implements Flyable {
	@Override
	public void fly() {
		System.out.println("bat fly");
	}

	@Override
	public void makeNoise() {
		System.out.println("bat make noise");
	}

	@Override
	public void eat() {
		System.out.println("bat eat");
	}

}
