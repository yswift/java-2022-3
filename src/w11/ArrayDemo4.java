package w11;

import java.util.Arrays;

public class ArrayDemo4 {
//	2、用0-100之间的随机数给数组元素赋值
//	static void init(int[] a) {
//		for (int v : a) {
//			v = (int) (Math.random() * 100);
//		}
//	}
	static void init(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) (Math.random() * 100);
		}
	}

//	3、遍历数组元素（从0开始访问数组的每个元素），并输出
	static void print(int[] a) {
		for (int v : a) {
			System.out.print(v + " ");
		}
		System.out.println();
	}

//	4、求数组元素的和
	static int sum(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}

//	5、求数组元素的最大值
	static int max(int[] a) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
		}
		return max;
	}
	
//	6、在数组中查找某个元素，找到返回元素的下标，没找到返回-1
	static int find(int[] a, int value) {
		for (int i = 0; i < a.length; i++) {
			if (a[i] == value) {
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
//		1、定义并创建10个元素的int型数组		
		int[] a = new int[10];
		
//		2、用0-100之间的随机数给数组元素赋值
		System.out.println();
		System.out.println("2、用0-100之间的随机数给数组元素赋值");
		init(a);
		
//		3、遍历数组元素（从0开始访问数组的每个元素），并输出
		System.out.println();
		System.out.println("3、遍历数组元素（从0开始访问数组的每个元素），并输出");
		print(a);
		
//		4、求数组元素的和
		System.out.println();
		System.out.println("4、求数组元素的和");
		int sum = sum(a);
		System.out.println("sum = " + sum);
		
//		5、求数组元素的最大值
		System.out.println();
		System.out.println("5、求数组元素的最大值");
		int max = max(a);
		System.out.println("max = " + max);
		
//		6、在数组中查找某个元素，找到返回元素的下标，没找到返回-1
		System.out.println();
		System.out.println("6、在数组中查找某个元素，找到返回元素的下标，没找到返回-1");
		System.out.println("查找：200");
		int idx = find(a, 200);
		System.out.println("结果：" + idx);
		System.out.println("查找：" + a[5]);
		idx = find(a, a[5]);
		System.out.println("结果：" + idx);
		
//		7、使用Arrays.sort 方法排序数组元素
		System.out.println();
		System.out.println("7、使用Arrays.sort 方法排序数组元素");
		System.out.println("结果：");
		Arrays.sort(a);
		print(a);
	}
}
