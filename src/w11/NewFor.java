package w11;

public class NewFor {
	// 用新型for循环求数组元素和
//	4、求数组元素的和
	static int sum(int[] a) {
		int sum = 0;
		for (int v : a) {
			sum += v;
		}
		return sum;
	}
	
	static int sum2(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			int v = a[i];
			sum += v;
		}
		return sum;
	}

	public static void main(String[] args) {
//		1、定义并创建10个元素的int型数组		
		int[] a = new int[10];
		
//		2、用0-100之间的随机数给数组元素赋值
		System.out.println();
		System.out.println("2、用0-100之间的随机数给数组元素赋值");
		ArrayDemo2.init(a);
		
//		3、遍历数组元素（从0开始访问数组的每个元素），并输出
		System.out.println();
		System.out.println("3、遍历数组元素（从0开始访问数组的每个元素），并输出");
		ArrayDemo2.print(a);
		
//		4、求数组元素的和
		System.out.println();
		System.out.println("4、用新型for循环求数组元素的和");
		int sum = sum(a);
		System.out.println("sum = " + sum);
	}
}
