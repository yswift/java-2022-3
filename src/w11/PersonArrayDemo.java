package w11;

import java.util.Arrays;
import java.util.Comparator;

public class PersonArrayDemo {
//	3、遍历数组元素（从0开始访问数组的每个元素），并输出Person的姓名
	static void print(Person[] people) {
		for (int i = 0; i < people.length; i++) {
			Person p = people[i];
			System.out.print(p.getName() + "  ");
		}
		System.out.println();
	}
	
//	4、求数组的身高和
	static double heightSum(Person[] people) {
		double sum = 0;
		for (Person p : people) {
			sum += p.getHeight();
		}
		return sum;
	}
	
//	5、找出数组中身高最高的人
	static Person heightMax(Person[] people) {
		Person max = people[0];
		for (Person p : people) {
			if (p.getHeight() > max.getHeight()) {
				max = p;
			}
		}
		return max;
	}
	
//	6、在数组中按姓名查找某个元素，找到返回元素，没找到返回null
	static Person find(Person[] people, String name) {
		for (Person p : people) {
			if (name.equals(p.getName())) {
				// 注意这里使用的是 equals 方法比较姓名相等
				return p;
			}
		}
		return null;
	}
	
	// 内部类
	static class HeighComparator implements Comparator<Person> {
		@Override
		public int compare(Person p1, Person p2) {
			return -(int)(100 * (p1.getHeight() - p2.getHeight()));
		}
	}
	
//	7、使用Arrays.sort 方法按身高排序数组元素
	static void sort(Person[] people) {
		// 使用内部类
		HeighComparator c = new HeighComparator();
		Arrays.sort(people, c);
		
//		Arrays.sort(people, new HeighComparator());
		
		// 这里用到了匿名内部类
//		Arrays.sort(people, new Comparator<Person>() {
//			@Override
//			public int compare(Person p1, Person p2) {
//				return (int)(100 * (p1.getHeight() - p2.getHeight()));
//			}
//		});
		
//		Arrays.sort(people, (Person p1, Person p2) -> {
//			return -(int)(100 * (p1.getHeight() - p2.getHeight()));
//		});
		
//		Arrays.sort(people, (Person p1, Person p2) -> 
//			-(int)(100 * (p1.getHeight() - p2.getHeight()))
//		);
		
//		Arrays.sort(people, (p1, p2) -> (int)(100 * (p1.getHeight() - p2.getHeight())));
	}

	
	public static void main(String[] args) {
//		1、定义并创建3个元素的Person型数组
		Person[] people = new Person[3];
		
//		2、给数组元素赋值
		people[0] = new Person("张三", 1.7, 65);
		people[1] = new Person("李四", 1.9, 60);
		people[2] = new Person("王五", 1.8, 70);
		
//		3、遍历数组元素（从0开始访问数组的每个元素），并输出姓名
		System.out.println("3、遍历数组元素（从0开始访问数组的每个元素），并输出姓名");
		PersonArrayDemo pd = new PersonArrayDemo();
		pd.print(people);
		System.out.println();
		
//		4、求数组的身高和
		System.out.println("4、求数组的身高和");
		double sum = heightSum(people);
		System.out.println("sum = " + sum);
		System.out.println();
		
//		5、找出数组中身高最高的人
		System.out.println("5、找出数组中身高最高的人");
		Person p = heightMax(people);
		System.out.println("身高最高的是：" + p.getName() + ", 身高: " + p.getHeight());
		System.out.println();
		
//		6、在数组中按姓名查找某个元素，找到返回元素，没找到返回null
		System.out.println("6、在数组中按姓名查找某个元素，找到返回元素，没找到返回null");
		System.out.println("在数组中查找姓名为张三的人");
		Person p1 = find(people, "张三");
		System.out.println("找到张三 = " + p1);
		
		System.out.println("在数组中查找姓名为赵六的人");
		Person p2 = find(people, "赵六");
		System.out.println("找到赵六 = " + p2);
		System.out.println();
		
//		7、使用Arrays.sort 方法按身高排序数组元素
		System.out.println("7、使用Arrays.sort 方法按身高排序数组元素");
		sort(people);
		System.out.println("排序结果: ");
		print(people);

	}

}
