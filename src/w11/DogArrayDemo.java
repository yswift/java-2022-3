package w11;

class Dog {
	int size;
}
public class DogArrayDemo {
	public static void main(String[] args) {
		Dog[] pets = new Dog[7];
		
		// 如果只创建的Dog数组，此时Dog数组的元素为null
		System.out.println("如果只创建的Dog数组，此时Dog数组的元素为null");
		System.out.println("pets[0] = " + pets[0]);
		System.out.println();
		
		System.out.println("放入两只狗");
		pets[0] = new Dog();
		pets[0].size = 10;
		pets[1] = new Dog();
		pets[1].size = 20;
		System.out.println("pets[0] = " + pets[0]);
		System.out.println();
	}
}
