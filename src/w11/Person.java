package w11;

public class Person {
	// 3个私有的实例变量
	// 姓名
	private String name;
	// 身高
	private double height;
	// 体重
	private double weight;
	
	public Person(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}

	// 每个变量的 getter/setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

//	@Override
//	public String toString() {
//		return "Person [name=" + name + ", height=" + height + ", weight=" + weight + "]";
//	}
	
	
}
