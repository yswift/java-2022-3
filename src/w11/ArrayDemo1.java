package w11;

import java.util.Arrays;

public class ArrayDemo1 {
	public static void main(String[] args) {
		// 1. 定义int类型数组
		int[] a1;
		
		// 2. 创建数组对象
		a1 = new int[10];
		
		// 输出a1, 方法1
		System.out.println(a1);
		System.out.println(new double[1]);
		
		// 输出a1, 方法2
		System.out.println(Arrays.toString(a1));
		
		// 3. 定义数组，并创建对象
		int[] a2 = new int[7];
		
		// 4. 定义数组，并赋初始值
		int[] a3 = {22, 4, 25, 9, 10, 20};
		System.out.println(Arrays.toString(a3));
		
		// 5. 数组长度（元素个数）
		System.out.println("a1.length = " + a1.length);
		System.out.println("a2.length = " + a2.length);
		System.out.println("a3.length = " + a3.length);
	}

}
