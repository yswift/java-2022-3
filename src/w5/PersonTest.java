package w5;

public class PersonTest {
	public static void main(String[] args) {
		Person p1 = new Person("张三");
		p1.info();
		
		Person p2 = p1;//new Person();
		p2.name = "李四";
		p2.info();
		
		p1.info();
	}

}
