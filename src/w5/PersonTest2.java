package w5;

public class PersonTest2 {
	static void rename(Person p) {
		p.name = "李四";
	}
	public static void main(String[] args) {
		Person p1 = new Person("张三");
		p1.info();
		System.out.println("call rename");
		rename(p1);
		p1.info();
	}
}
