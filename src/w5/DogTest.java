package w5;

public class DogTest {
	public static void main(String[] args) {
		Dog d = new Dog();
		System.out.println("d.size = " + d.size);
		d.bark();
		
		Dog d2 = new Dog(20);
		System.out.println("d2.size = " + d2.size);
		d2.bark();
	}

}
