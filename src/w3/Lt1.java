package w3;

public class Lt1 {
	public static void main(String[] args) {
		byte b = 34;
		int i = 34;
		
		b = i; // 出错，溢出
		b = (byte) i; // 显示
		i = b; // 自动，隐式
	}

}
