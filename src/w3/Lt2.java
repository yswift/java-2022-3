package w3;

public class Lt2 {
	public static void main(String[] args) {
//		算术运算符(+-*/)
		int a = 7;
		int b = 9;
		
		System.out.println("a + b = " + (a+b));
		System.out.println("a - b = " + (a-b));
		System.out.println("a * b = " + (a*b));
		System.out.println("a / b = " + (a/b));
		System.out.println("a / b = " + ((double)a/b));
		System.out.println("a / b = " + (1.0*a/b));
		System.out.println("a % b = " + (a%b));
		
//		关系运算符(>,>=,<,<=,==,!=)
		System.out.println("a > b ? " + (a>b));
		System.out.println("a < b ? " + (a<b));
		System.out.println("a == b ? " + (a==b));
		System.out.println("a != b ? " + (a!=b));
		
//		布尔逻辑运算符(&&,||,!)
		boolean b1 = true;
		boolean b2 = true;
		boolean b3 = false;
		System.out.println("b1 && b2 = " + (b1 && b2));
		System.out.println("b1 && b3 = " + (b1 && b3));
		System.out.println("b1 || b2 = " + (b1 || b2));
		System.out.println("b1 || b3 = " + (b1 || b3));
		System.out.println("!b1 = " + (!b1));
		System.out.println("!b3 = " + (!b3));
		
//		位运算符(&,|,!,^)
//		赋值类运算符(=,+=,-=,*=)
		System.out.println("a += b， a = " + (a+=b));
		System.out.println("a -= b， a = " + (a-=b));
		System.out.println("a *= b， a = " + (a*=b));
		
		a = 7;
		b = 9;
		a *= b + 3;
		System.out.println("a = " + a);
		
//		条件运算符(?:)
	}

}
