package w12;

public class VarDemo {
	public static void main(String[] args) {
		// java5 以前装箱
		Integer a1 = Integer.valueOf(5);
		// java5 以后自动装箱
		Integer a2 = 5;
		System.out.println("a1 = " + a1);
		System.out.println("a2 = " + a2);

		// java5 以前拆箱
		int a4 = a1.intValue();
		long l1 = a1.longValue();
		// java5 以后自动装箱
		int a5 = a1;
		System.out.println("a4 = " + a4);
		System.out.println("a5 = " + a5);
		
		// 比较相等
		Integer a6 = Integer.valueOf(500);
		Integer a7 = Integer.valueOf(500);
		System.out.println("用 == 比较相等？" + (a6 == a7));
		System.out.println("用 equals 比较相等？" + (a6.equals(a7)));
		
		// 基本类型和字符串之间的转换
		String s1 = "123";
		int a8 = Integer.parseInt(s1);
		
		String s2 = "123.456";
		double a9 = Double.parseDouble(s2);
		
		String s3 = String.valueOf(a8);
		String s4 = String.valueOf(a9);
	}

}
