package w4_2;

public class CircleTest {
	public static void main(String[] args) {
		Circle c1 = new Circle();
		c1.radius = 10;
		
		Circle c2 = new Circle();
		c2.radius = 20;
		
		double area1 = c1.getArea();
		double area2 = c2.getArea();
		
		System.out.println("area1 = " + area1);
		System.out.println("area2 = " + area2 );
	}

}
