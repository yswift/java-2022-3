package w10;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Lx2 {
	public static void main(String[] args) {
		LocalDateTime dt = LocalDateTime.now();
//		2021-11-5 10:35
		DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		System.out.println(f.format(dt));
		
		LocalDateTime dt2 = LocalDateTime.parse("2022-12-30 00:00", f);
		
//		2021-11-5 10:35:10
//
//		2021年11月5日
		f = DateTimeFormatter.ofPattern("yyyy年MM月d日");
		System.out.println(f.format(dt));
//		2021年11月05日
//		2021年11月5日 周五
//		2021年11月5日 星期五
		f = DateTimeFormatter.ofPattern("yyyy年MM月d日 EEEE");
		System.out.println(f.format(dt));

	}

}
